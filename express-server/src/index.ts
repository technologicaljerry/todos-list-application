import "express-async-errors";
import express, { Request, Response } from "express";
import cors from "cors";
import connectDB from "./config/db.config";
import todoRouter from './routes/todo.routes';
import dotenv from 'dotenv';

dotenv.config();

const server = express();
const port = process.env.PORT || 9090;

// Enable CORS
server.use(cors({
    origin: 'http://localhost:3000', // Allow requests from this origin
    methods: ['GET', 'POST', 'PUT', 'DELETE'], // Specify allowed methods if needed
    credentials: true // Include credentials if required by your backend
}));

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

// Health check route
server.get('/check', (req, res) => {
    console.log('Server Health check pass!.....');
    res.send("<h1>Todo List using TypeScript, Express, and MongoDB</h1>");
});

// Login route
server.post('/login', (req: Request, res: Response) => {
    const { username, password } = req.body;

    // Temporary mock authentication (replace with real authentication logic)
    if (username === "admin" && password === "pass") {
        console.log("Login successful");
        res.status(200).json({ message: "Login successful, redirecting to /todos" });
    } else {
        res.status(401).json({ message: "Invalid credentials" });
    }
});

// Todos route
server.use('/todos', todoRouter);

const startDB = async () => {
    try {
        await connectDB(process.env.MONGO_URI as string);
        console.log('MongoDB is connected!!!');
        server.listen(port, () => {
            console.log(`Server is listening on port:${port}`);
        });
    } catch (error: any) {
        console.log('Error in DB Connection', error);
    }
};

startDB();
