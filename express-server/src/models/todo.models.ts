import { Schema, model } from 'mongoose';

// Creating an interface
interface Todos {
    title: string,
    body: string,
    category: string,
    subCategory: string,
    completed: boolean
}

const todoSchema = new Schema<Todos>({
    title: {
        type: String,
        required: [true, "Title should not be empty!"],
        unique: true
    },
    body: {
        type: String,
        required: [true, "Body should not be empty!"],
        unique: false
    },
    category: {
        type: String,
        required: [true, "Category should not be empty!"],
        unique: false
    },
    subCategory: {
        type: String,
        required: [true, "Sub Category should not be empty!"],
        unique: false
    },
    completed: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });


export const Todo = model<Todos>('Todo', todoSchema);