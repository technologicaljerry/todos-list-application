import mongoose from "mongoose";

const connectDB = (url: any) => {
    mongoose.connect("mongodb://localhost:27017/todos-list-application");
}

export default connectDB;